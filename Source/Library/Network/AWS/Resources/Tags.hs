{-# Language TemplateHaskell #-}

{-|
Module:             Types.Filters.Tags
Description:        AWS resource tags.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Types.Filters.Tags (
    EC2Tag,
    ec2Tag,
    HasTags(..)
    ) where

import Lawless
import Text hiding (text)
import Textual
import Printer
import Types.Filters.Filters
import Types.Filters.Classes
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Set

-- | A set of AWS Tags that can be converted to filters.
newtype Tags = Tags {unTags ∷ Filters}
    deriving (Eq, Ord, Show, Monoid)

-- | Construct a single tag with empty values.
tags ∷ Filters → Tags
tags = Tags

-- | Print the 'Tags' nearly like AWS, but still human-readable.
instance Printable Tags where
    print  = print ∘ view toFilters

-- | Transform 'Tags' to 'Filters' by prepending the "tag:" prefix to the key.
instance ToFilters Tags where
    toFilters =
        to $ ifoldMapOf ifolded (\k vs → filtName ("tag:" <> k)  vs) unTags

class HasTags a where
    getTags ∷ Getter a Tags
