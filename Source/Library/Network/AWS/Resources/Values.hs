{-# OPTIONS_GHC -fno-warn-orphans #-}

{-|
Module:             Types.Filters.Values
Description:        AWS resource values.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Types.Filters.Values (
    OwnerId,
    Name
    )where

import Types.Filters.Filters
import Types.Filters.Classes
import Lawless
import Text hiding (text)
import Network.AWS.EC2
import Data.Word
import Numeric
import Textual
import Printer

-- | The owner of a resource.
newtype OwnerId = OwnerId Word64
    deriving (Eq, Ord, Show, Bounded, Enum, Integral, Num, Real)

-- | Print an 'OwnerId' with 12 digits, adding leading 0s when
-- necessary.
instance Printable OwnerId where
    print a =
        let
            t ∷ [Char]
            t = nnDecimal $ a

            z = '0' ^.. replicated (12 - lengthOf traversed t)
        in
            print $ z <> t

instance IsFilter OwnerId where
    filtName = to $ const "owner-id"
    filtValue =
        let
            s oid = showInt oid ""
            n oid = 12 - lengthOf traversed (s oid)
            p oid = '0' ^.. repeated ^.. taking (n oid) traversed
            v (OwnerId oid) = (p oid ++ s oid) ^. packed
        in
            to v

instance IsMultiFilter OwnerId
instance ToFilters OwnerId

-- | A name.
newtype Name = Name {unName ∷ Text} deriving (Eq, Ord, Show)

instance IsFilter Name where
    filtName = to $ const "name"
    filtValue = to $ unName

instance IsMultiFilter Name
instance ToFilters Name

-- * Orphan instances for Amazonka values.

instance IsFilter ArchitectureValues where
    name = to $ const "architecture"
    value = to $
        \case
            I386 → "i386"
            X86_64 → "x86_64"

instance IsMultiFilter ArchitectureValues
instance ToFilters ArchitectureValues

instance IsFilter VirtualizationType where
    name = to $ const "virtualization-type"
    value = to $
        \case
            HVM → "hvm"
            Paravirtual → "paravirtual"

instance IsMultiFilter VirtualizationType
instance ToFilters VirtualizationType

instance IsFilter DeviceType where
    name = to $ const "root-device-type"
    value = to $
        \case
            EBS → "ebs"
            InstanceStore → "instance-store"

instance IsMultiFilter DeviceType
instance ToFilters DeviceType
