{-|
Module:             Types.Filters.Classes
Description:        Typeclasses for converting between values that can be 'Filter's and 'Filter's.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@navipointgenomics.com>
Stability:          experimental
Portability:        POSIX
-}

module Types.Filters.Classes (
    ToFilters(..),
    IsFilter(..),
    IsMultiFilter(..),
    AWSRequestFilters(..)
    ) where

import Lawless
import Set
import Text hiding (text)
import Network.AWS.Types
import qualified Network.AWS.EC2 as E
import Types.Filters.Filters

default (Text)

-- * Classes for converting to 'Filters' and ultimately ['Filter'].

-- ** Basic 'Filters' Classes

-- | These are the classes used to distinguish the various kinds of
-- filters from each other, and to ultimately convert them all to
-- '[Filter]' for 'AWSRequest's.
--
-- The simplest filter is an 'IsFilter', which is a simple FilterName and
-- FilterValue. This is automatically converted to a 'Filter' as needed.

-- | Anything that can be turned into an AWS request filter. This also
-- requires 'ToFilters' for the 'RequestFilters' class, and
-- 'IsMultiFilter' for the default conversion to a 'Filters'.
class (IsMultiFilter a, ToFilters a) ⇒ IsFilter a where
    -- | The tag name
    filtName ∷ Getter a FilterName

    -- | The Single Value Of This Tag.
    filtValue ∷ Getter a FilterValue

-- | A 'Filter' with a single name that can have multiple values.
class (ToFilters a) ⇒ IsMultiFilter a where
    -- | The tag key. By default, if it's an 'IsFilter' instance,
    -- it'll use the key from there.
    mfName ∷ Getter a FilterName

    default mfName ∷ IsFilter a ⇒ Getter a FilterName
    mfName = to $ view filtName

    -- | Returns the 'IsMultiFilter' 'FilterValues'. The default for
    -- 'IsFilter' instances is to return the singular 'FValue' as an
    -- 'FilterValues'.
    mfValues ∷ Getter a FilterValues
    default mfValues ∷ IsFilter a ⇒ Getter a FilterValues
    mfValues = to $ view filterValues ∘ view sing ∘ view filtValue

-- | Anything that can be converted to 'Filters'
class ToFilters a where
    -- | Convert a value to a 'Filters'. The default method for
    -- 'IsMultiFilter' will convert it to a singleton.
    toFilters ∷ Getter a Filters
    default toFilters ∷ IsMultiFilter a ⇒ Getter a Filters
    toFilters =
        to $ \a → filters (a ^. mfName) (a ^. mfValues)

class AWSRequest q ⇒ AWSRequestFilters q where
    -- | Go between the '[E.Filter]' and 'Filters' of the 'AWSRequest'.
    reqFilters ∷ Lens' q [E.Filter]

    setFilters ∷ q → Filters → q
    setFilters req fs =
        req & reqFilters <>~ (fs ^. awsFilters)
