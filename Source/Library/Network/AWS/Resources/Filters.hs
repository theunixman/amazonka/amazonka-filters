{-|
Module:             Network.AWS.Resources.Filters.Filters
Description:        Parameterized filter value and 'Set' with 'Functor' and 'Foldable'.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX

Both a value and collection of values, parameterized, and 'Textual' so
they can form a list of 'Filter's.
-}

module Network.AWS.Resources.Filters (
    Flt,
    Flts,
    IsFilter(..),
    Filter,
    filter,
    Filters,
    filters,
    awsFilters
    ) where

import Lawless
import Textual
import Set hiding ((∅))
import qualified Network.AWS.EC2 as E
import Text
import Data.Functor.Coyoneda

-- * The Constraints on all 'Filter's

-- | The context of a 'Filter' value without 'IsFilter'.
type Flt' v = (Eq v, Ord v, Show v, Printable v, Textual v)

-- | The context of an individual 'Filter' value.
type Flt v = (Flt' v, IsFilter v, Eq v, Ord v, Show v, Printable v, Textual v)

-- | Context for value collection. 'Filters' is a 'Monoid' so that
-- '(⊕)' can add values to the set of 'Filters'.
type Flts v = (Flt v, Monoid (Filters v))

-- * The 'IsFilter' Class

class Flt' v ⇒ IsFilter v where
    -- | Returns the AWS request name of this filter as a 'Text'. This
    -- is used by the 'filtName' 'Getter' to create the 'FilterName'
    -- for the underlying 'Filter'.
    filterName ∷ v → Text

-- * A Filter Value

-- | The value type of a filter. This is the value itself wrapped in a
-- 'Coyoneda'. This permits containers with constrained values ('Flt')
-- so we can:
--
-- 1. Mapping over an 'Filter' @v@ to get a 'Text', which is what AWS
-- expects. The 'Printable' instance would normally be used for
-- this. See 'awsFilter'.
--
-- 2. Mapping over an 'Filter' 'Text' to get an 'Filter' @v@ using
--'parseText' (from the 'Textual' instance). This can then be used to
--parse input and ensure we have a valid value for a given 'Filter'
newtype Filter v = Filter {runFString ∷ Coyoneda Identity v}
    deriving (Functor)

-- | 'Iso' between the unedrlying value @v@ and the 'Filter'
-- container.
filter ∷ Iso' (Filter v) v
filter = iso (runIdentity ∘ lowerCoyoneda ∘ runFString) (Filter ∘ liftCoyoneda ∘ Identity)

instance Eq v ⇒ Eq (Filter v) where
    a == b = (a ^. filter) ≡ (b ^. filter)

instance Ord v ⇒ Ord (Filter v) where
    a `compare` b = (a ^. filter) `compare` (b ^. filter)

instance Show v ⇒ Show (Filter v) where
    show s = "(" <> (show $ s ^. filter) <> ") ^.re filter"

instance Printable v ⇒ Printable (Filter v) where
    print = print ∘ view filter

-- | 'Filter' itself uses the 'Textual' instance of the underlying
-- type.
instance Flt v ⇒ Textual (Filter v) where
    textual = review filter <$> textual

-- * A 'Set' of 'Filter's

-- | This is a 'Set' of 'Filter's that's used to represent the
-- disjunction of values for the AWS API request.
newtype Filters v = Filters {unFilters ∷ Set (Filter v)}
    deriving (Eq, Ord, Show, Monoid)

-- | We can convert 'Filters' into any 'Monoid'. This is used to
-- convert from 'Filters' to '[Text]' for AWS requests, for example.
instance Foldable Filters where
    foldMap f = foldMap (f ∘ view filter) ∘ unFilters

-- | 'Iso' between a 'Set' and an 'Filters' of @v@s.
filters ∷ Flts v ⇒ Iso'  (Filters v) (Set v)
filters =
    let
        fvs ∷ Flts v ⇒ Filters v → Set v
        fvs = (foldMapOf folded (view sing ∘ view filter)) ∘ unFilters

        vsf ∷ Flts v ⇒ Set v → Filters v
        vsf = (foldMapOf folded (Filters ∘ view sing ∘ review filter))
    in
        iso fvs vsf

-- | Translate 'Filters' @v@ to an 'E.Filter' with all the requisite values.
awsFilters ∷ ∀ v. Flts v ⇒ Getter (Filters v) (E.Filter)
awsFilters = let n = filterName (undefined ∷ v) in
    to $ \fs → E.filter' n & E.fValues <>~ foldMapOf folded ((:[]) ∘ toText) fs
