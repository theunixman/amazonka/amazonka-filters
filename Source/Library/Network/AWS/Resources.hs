{-|
Module:             Network.AWS.Resources
Description:        Various resources for the Amazonka library.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@navipointgenomics.com>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Resources (
    module Network.AWS.Resources.Filters
    ) where

import Network.AWS.Resources.Filters
