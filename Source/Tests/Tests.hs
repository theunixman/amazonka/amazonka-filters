import qualified FString

import Test.Tasty

main = defaultMain tests

tests :: TestTree
tests = testGroup "Resources" [FString.tests]
