{-# Language TemplateHaskell #-}

{-|
Module:             FString
Description:        Quickcheck test cases for various 'FString' properties.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@navipointgenomics.com>
Stability:          experimental
Portability:        POSIX
-}

module FString where

import Lawless hiding (elements)
import Test.Tasty
import Test.Tasty.QuickCheck
import Test.Tasty.TH
import Network.AWS.Resources
import Textual
import Parser

-- | Define a filter type, an 'Architecture'.
data Architecture = X86 | AMD64 deriving (Eq, Show, Ord)
instance Printable Architecture where
    print = \case
        X86 → "i386"
        AMD64 → "x86_64"

instance Textual Architecture where
    textual = (X86 <$ text "i386") <|> (AMD64 <$ text "x86_64")

instance Arbitrary Architecture where
    arbitrary = elements [X86, AMD64]

prop_CheckTextual (arch ∷ Architecture) =
    (fromText ∘ toText) arch === Just arch

prop_CheckFunctor (arch ∷ Architecture) =
    let
        a = arch ^. re fstring
    in
        (toText <$> a) ^. fstring === toText a

tests :: TestTree
tests = $(testGroupGenerator)
